# LaundryAlertCache

Proof-of-concept Spring Boot application for the LaundryAlert API (since the default
API is inefficient). This application is incomplete and will not be completed
unless permission is given by Advanced Laundry Devices, Inc. It allows the
[LaundryAlertPlus](https://gitlab.com/gjabell/laundryalertplus) Android app to
query for information in a more efficient way.

**Disclaimer: This is an unofficial, proof-of-concept cache and is in no way affiliated
with Advanced Laundry Devices, Inc. The LaundryAlert terms of use forbid the use
of their API without permission. I am not responsible for breach of these terms
of use as a result of using this software.**