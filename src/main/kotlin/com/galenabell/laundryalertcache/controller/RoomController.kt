package com.galenabell.laundryalertcache.controller

import com.galenabell.laundryalertcache.db.MachineDao
import com.galenabell.laundryalertcache.db.RoomDao
import com.galenabell.laundryalertcache.models.Machine
import com.galenabell.laundryalertcache.models.Room
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class RoomController {
    @Autowired
    lateinit var roomDao: RoomDao

    @Autowired
    lateinit var machineDao: MachineDao

    @GetMapping("/rooms")
    fun getRooms(): List<Room> = roomDao.findAll()

    @GetMapping("/rooms/{name}")
    fun getRoom(@PathVariable(value = "name") name: String): ResponseEntity<Room> = roomDao.findAll()
            .find { r -> r.name == name }
            .let { r ->
                r?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
            }

    @GetMapping("/rooms/{name}/machines")
    fun getMachinesForRoom(@PathVariable(value = "name") name: String): List<Machine> = machineDao.findAll()
            .filter { m -> m.roomName == name }
}
