package com.galenabell.laundryalertcache.controller

import com.galenabell.laundryalertcache.db.LocationDao
import com.galenabell.laundryalertcache.db.RoomDao
import com.galenabell.laundryalertcache.models.Location
import com.galenabell.laundryalertcache.models.Room
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class LocationController {
    @Autowired
    lateinit var locationDao: LocationDao

    @Autowired
    lateinit var roomDao: RoomDao

    @GetMapping("/locations")
    fun getLocations(): List<Location> = locationDao.findAll()

    @GetMapping("/locations/{code}")
    fun getLocation(@PathVariable(value = "code") code: String): ResponseEntity<Location> = locationDao.findAll()
            .find { l -> l.code == code }
            .let { l ->
                l?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
            }

    @GetMapping("/locations/{code}/rooms")
    fun getRoomsForLocation(@PathVariable(value = "code") code: String): List<Room> = roomDao.findAll()
            .filter { r -> r.locationCode == code }
}
