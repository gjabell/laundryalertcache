package com.galenabell.laundryalertcache.controller

import com.galenabell.laundryalertcache.db.MachineDao
import com.galenabell.laundryalertcache.models.Machine
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class MachineController {
    @Autowired
    lateinit var machineDao: MachineDao

    @GetMapping("/machines")
    fun getMachines(): List<Machine> = machineDao.findAll()

    @GetMapping("/machines/{name}/{label}")
    fun getMachineForRoom(@PathVariable(value = "name") name: String, @PathVariable(value = "label") label: String): ResponseEntity<Machine> {
        return machineDao.findAll()
                .find { m -> m.roomName == name && m.label == label }
                .let { m ->
                    m?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
                }
    }

    @GetMapping("/machines/{name}")
    fun getMachinesForRoom(@PathVariable(value = "name") name: String): List<Machine> {
        return machineDao.findAll().filter { machine -> machine.roomName == name }
    }
}
