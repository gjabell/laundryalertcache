package com.galenabell.laundryalertcache.util

import com.galenabell.laundryalertcache.db.LocationDao
import com.galenabell.laundryalertcache.db.MachineDao
import com.galenabell.laundryalertcache.db.RoomDao
import com.galenabell.laundryalertcache.models.Location
import com.galenabell.laundryalertcache.models.Machine
import com.galenabell.laundryalertcache.models.Room
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.Request
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

@Component
class DataInitializer {
    @Autowired
    lateinit var locationDao: LocationDao

    @Autowired
    lateinit var roomDao: RoomDao

    @Autowired
    lateinit var machineDao: MachineDao

    private val logger = LoggerFactory.getLogger(javaClass)

    private val gson = GsonBuilder()
            .registerTypeAdapter(Location::class.java, LocationDeserializer())
            .registerTypeAdapter(RoomWrapper::class.java, RoomDeserializer())
            .create()
    private val baseUrl = "http://ec2-23-23-147-128.compute-1.amazonaws.com/homes"
    private val locationsPath = "/nearbylocation"
    private val dataPath = "/mydata/%s"
    private val client = OkHttpClient()
    private val locationsRequest = Request.Builder()
            .url(baseUrl + locationsPath)
            .build()

    @EventListener
    fun appReady(event: ApplicationReadyEvent) {
        fetchData()
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    fun dataFetcher() {
        fetchData()
    }

    private fun fetchData() {
        val locationResponse = client.newCall(locationsRequest).execute()
        val locations = ConcurrentLinkedQueue(gson.fromJson<Array<Location>>(locationResponse.body()?.string(), Array<Location>::class.java).toList())
        logger.debug("Fetched locations")
        val rooms = ArrayList<Room>()
        val machines = ArrayList<Machine>()
        locationDao.deleteAll()
        locationDao.saveAll(locations)
        var location = locations.poll()
        while (location != null) {
            val roomRequest = Request.Builder()
                    .url(baseUrl + String.format(dataPath, location.code))
                    .build()
            val roomResponse = client.newCall(roomRequest).execute()
            val roomString = roomResponse.body()?.string()
            try {
                val roomWrapper = gson.fromJson<RoomWrapper>(roomString, RoomWrapper::class.java)
                logger.debug("Fetched ${location.code}")

                roomWrapper.rooms.forEach { r -> r.locationCode = location.code }
                rooms.addAll(roomWrapper.rooms)
                machines.addAll(roomWrapper.machines)
            } catch (ignored: IllegalArgumentException) {
                logger.warn("Failed to download ${location.code}, retrying")
                locations.add(location)
            }
            location = locations.poll()
        }

        roomDao.deleteAll()
        roomDao.saveAll(rooms)

        machineDao.deleteAll()
        machineDao.saveAll(machines)
    }
}
