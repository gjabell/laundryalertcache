package com.galenabell.laundryalertcache.util

import com.galenabell.laundryalertcache.models.Machine
import com.galenabell.laundryalertcache.models.Room
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class RoomDeserializer : JsonDeserializer<RoomWrapper> {
    private val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

    override fun deserialize(e: JsonElement?, type: Type?, context: JsonDeserializationContext?): RoomWrapper {
        val w = e?.asJsonObject?.get("location")?.asJsonObject
        val rooms = ArrayList<Room>()
        val machines = ArrayList<Machine>()
        val roomsJson = w?.get("rooms") ?: JsonNull.INSTANCE
        if (roomsJson.isJsonNull) throw IllegalArgumentException("Rooms was null")
        w?.get("rooms")?.asJsonArray?.forEach { rawRoom ->
            val room = deserializeRoom(rawRoom)
            rooms.add(room)
            rawRoom.asJsonObject?.get("machines")?.asJsonArray?.forEach { machine -> machines.add(deserializeMachine(machine, room)) }
        }
        return RoomWrapper(rooms, machines)
    }

    private fun deserializeRoom(e: JsonElement?): Room {
        val r = e?.asJsonObject
        val name = r?.get("name")?.asString ?: ""
        return Room(name)
    }

    private fun deserializeMachine(e: JsonElement?, room: Room): Machine {
        val m = e?.asJsonObject
        val label = m?.get("label")?.asString ?: ""
        val description = m?.get("description")?.asString ?: ""
        val status = m?.get("status")?.asString ?: ""
        val startTime = format.parse(m?.get("startTime")?.asString)
        val timeRemaining = m?.get("timeRemaining")?.asString ?: ""
        return Machine(room.name, label, description, status, startTime, timeRemaining)
    }
}
