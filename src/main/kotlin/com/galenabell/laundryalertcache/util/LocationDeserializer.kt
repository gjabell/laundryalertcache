package com.galenabell.laundryalertcache.util

import com.galenabell.laundryalertcache.models.Location
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class LocationDeserializer : JsonDeserializer<Location> {
    override fun deserialize(e: JsonElement?, type: Type?, context: JsonDeserializationContext?): Location {
        val l = e?.asJsonObject?.get("location_masters")?.asJsonObject
        val code = l?.get("code")?.asString ?: ""
        val name = l?.get("name")?.asString ?: ""
        return Location(code, name)
    }
}
