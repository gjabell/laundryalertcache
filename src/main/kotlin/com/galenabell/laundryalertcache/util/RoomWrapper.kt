package com.galenabell.laundryalertcache.util

import com.galenabell.laundryalertcache.models.Machine
import com.galenabell.laundryalertcache.models.Room

data class RoomWrapper(var rooms: List<Room>, var machines: List<Machine>)
