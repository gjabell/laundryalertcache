package com.galenabell.laundryalertcache.app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@ComponentScan(basePackages = ["com.galenabell.laundryalertcache"])
@EnableJpaRepositories(basePackages = ["com.galenabell.laundryalertcache.db"])
@EntityScan(value = ["com.galenabell.laundryalertcache.models"])
@EnableScheduling
class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
