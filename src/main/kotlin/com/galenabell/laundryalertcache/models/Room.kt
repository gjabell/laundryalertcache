package com.galenabell.laundryalertcache.models

import org.hibernate.annotations.Formula
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "rooms")
data class Room(
        @Id
        @get: NotBlank
        val name: String = "",

        @Column(name = "location_code")
        @get: NotBlank
        var locationCode: String = "",

        @Column(name = "num_machines")
        @Formula("select count(*) from machines m where m.room_name = name")
        val numMachines: Int = 0,

        @Column(name = "num_available")
        @Formula("select count(*) from machines m where m.room_name = name and m.status = 'Available'")
        val numAvailable: Int = 0)
