package com.galenabell.laundryalertcache.models

import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "machines")
data class Machine(
        @Column(name = "room_name")
        @get: NotBlank
        val roomName: String = "",

        @get: NotBlank
        val label: String = "",

        @get: NotBlank
        val description: String = "",

        @get: NotBlank
        val status: String = "",

        @Column(name = "start_time")
        @get: NotNull
        val startTime: Date = Date(),

        @Column(name = "time_remaining")
        @get: NotBlank
        val timeRemaining: String = "",

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0)
