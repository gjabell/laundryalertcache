package com.galenabell.laundryalertcache.models

import org.hibernate.annotations.Formula
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "locations")
data class Location(
        @Id
        @get: NotBlank
        val code: String = "",

        @get: NotBlank
        val name: String = "",

        @Column(name = "num_rooms")
        @Formula("select count(*) from rooms r where r.location_code = code")
        val numRooms: Int = 0)
