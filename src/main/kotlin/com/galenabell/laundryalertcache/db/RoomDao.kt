package com.galenabell.laundryalertcache.db

import com.galenabell.laundryalertcache.models.Room
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoomDao : JpaRepository<Room, String>
