package com.galenabell.laundryalertcache.db

import com.galenabell.laundryalertcache.models.Machine
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MachineDao : JpaRepository<Machine, Long>
